import { Post } from './Post';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-post-list-item-component',
  templateUrl: './post-list-item-component.component.html',
  styleUrls: ['./post-list-item-component.component.css']
})
export class PostListItemComponentComponent implements OnInit {
   @Input() postItem: Post;
  constructor() {}
  ngOnInit() {
  }
onLike() {
  this.postItem.loveIts ++;
}
  onDislike() {
  this.postItem.loveIts --;
}
}
