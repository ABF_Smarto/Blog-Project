import { Post } from './post-list-item-component/Post';
import { Component } from '@angular/core';
import { PostListItemComponentComponent } from './post-list-item-component/post-list-item-component.component';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
    posts = [new Post('Post 1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do ei.', 3),
             new Post('Post 2', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do ei.', -1),
             new Post('Post 3', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do ei.', 0),
             new Post('Post 4', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do ei.', 1)

];

}
